# Putty - CLI (Command Line Interface)

## Instalación
Actualización de repositorios e instalación:

```bash
sudo apt update
sudo apt install putty
```

## Ejecución

Para un correcto funcionamiento del programa, se recomienda ejecutarlo con permisos de privilegios:

```bash
sudo putty
```
