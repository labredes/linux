# Reenvío de paquetes

Por defecto, una PC con Linux no se encuentra configurada para funcionar como enrutador. Para utilizarla como router, es necesario habilitar el reenvío de paquetes:

```bash
sudo echo "numero" > /proc/sys/net/ipv4/ip_forward
```

Donde: | |
-- | -- |
**numero** = 1 (habilita el reenvío) / 0 (deshabilita el reenvío) | |
