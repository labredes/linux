# Direcciones MAC - Tabla ARP - Comando: arp

El comando arp se encuentra disponible en algunas distribuciones antiguas o firmware específicos de Linux.
En caso de no encontrarla, instalarlo mediante:

```bash
sudo apt install net-tools
```

## Visualización tabla ARP

Mediante el comando **arp -n** se lista la tabla ARP del dispositivo, de la forma en que se muestra a continuación:

```bash 
arp -n

Address     HWtype     HWaddress             Flags Mask     Iface
"x.x.x.x"   "tipo"     "xx:xx:xx:xx:xx:xx"     C            "interfaz"
"x.x.x.x"              "(incomplete)"                       "interfaz"
```

Donde: | |
-- | --
**x.x.x.x** = dirección IP | **xx:xx:xx:xx:xx:xx** = dirección MAC de la interfaz
**tipo** = tipo de dirección de capa 2 | **(incomplete)** = se desconoce la dirección MAC
**interfaz** = nombre lógico de la interfaz hacia esa IP |

## Limpieza de una entrada en la tabla ARP

A partir del siguiente comando, es posible eliminar una entrada de la tabla ARP:

```bash
sudo arp -d "x.x.x.x"
```

Donde: | |
-- | --
**x.x.x.x**= dirección IP a eliminar dentro de la tabla

## Carga manual de una dirección MAC a la tabla ARP

A partir del siguiente comando, es posible cargar en forma estática una asociación entre una dirección IP y dirección MAC asociada a la interfaz donde se encuentra configurada dicha IP:

```bash
sudo arp -s "x.x.x.x" "xx:xx:xx:xx:xx:xx"
```

Donde: | |
-- | --
**x.x.x.x** = dirección IP | **xx:xx:xx:xx:xx:xx** = dirección MAC de la interfaz que tiene asociada la IP
