# PING

El comando ping permite entre otras cosas, comprobar la conectividad entre dispositivos y conocer parcialmente la calidad del enlace entre los mismos, presentando estadísticos de pérdida de paquetes y retardos.

## Instalación

En caso que el comando no se encuentre disponible, puede instalarse siguiendo los pasos:

```bash
sudo apt update
sudo apt install iputils-ping
```

## Modo de uso

```bash
ping "destino"
```

Donde: ||
:-: | :-: |
**destino** = Dirección IP o dominio destino | |