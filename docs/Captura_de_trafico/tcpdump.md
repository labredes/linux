# tcpdump

**tcpdump** es una herramienta de captura de tráfico que se implementa desde un terminal.

Sitio web oficial: [enlace](https://www.tcpdump.org/manpages/tcpdump.1.html)

## Modo de uso

```bash
tcpdump -c "cantidad" -C "tamaño_archivo" -i "interfaz" -s "tamaño_t_p" -w "archivo"
```

Donde: ||
-- | -- |
**cantidad** = indica la cantidad máxima de tramas/paquetes a capturar | **tamaño_archivo** = tamaño del archivo de salida indicado en la opción **-w**
**interfaz** = nombre de la interfaz cuyo tráfico se desea capturar | **tamaño_t_p** = indica el tamaño máximo a capturar por cada trama/paquete 
**archivo** = nombre del archivo donde se guarda la captura |

En caso que la tasa de transferencia sea alta o que se capture durante mucho tiempo el flujo de datos, el archivo resultante puede resultar de gran tamaño. Por ello, si es de interés analizar sólo los encabezados o el inicio de las tramas o paquetes, se recomienda truncar cada mensaje, con el uso de la opción **-s**

## Filtros
