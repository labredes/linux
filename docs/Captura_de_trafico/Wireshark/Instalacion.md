# Instalación Wireshark

## Linux

El instalador se encuentra disponible en los repositorios de Ubuntu, con lo que se puede instalar directamente con el comando **apt**:

```bash
sudo apt update
sudo apt install wireshark
```

## Windows

El instalador puede descargarse desde el siguiente [enlace](https://www.wireshark.org/#download).
