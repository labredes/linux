# Direcciones IP - Comando ip address

**ip address** se encuentra disponible en versiones recientes de Linux. 

De ser necesaria su instalación, adicionar el paquete **iproute2** como se muestra a continuación. 

De no encontrarse en el sistema, puede utilizarse el [comando ifconfig](/linux/Direcciones_IP/ifconfig).

## Instalación

De no encontrarse el comando **ip**, se puede instalar con:

```bash
sudo apt update
sudo apt install iproute2
```

## Visualización de configuración IP

### Visualización abreviada

```
ip -c -br -4 address
```

El argumento **-c** permite resaltar parte de la configuración con colores. Por otro lado **-br** permite organizar el resultado en una tabla y **-4**, lista sólo las direcciones IPv4.

### Visualización Completa

```
ip -c address
```

El argumento **-c** permite resaltar parte de la configuración con colores. Simplifica identificar nombre de interfaces y direcciones.

## Asignación de una dirección IP a una interfaz

```bash
sudo ip address "add/del" dev "interfaz" "x.x.x.x"/"y"
```

Donde: | | |
-- | -- | --
**add** = adicionar una dirección IP | **del** = borra una dirección IP anterior
**interfaz** = nombre lógico de la interfaz | **x.x.x.x** = dirección IP del dispositivo
**y** = tamaño (en decimal) de la máscara de red | |

Luego de asignar la configuración, se recomienda visualizar sus resultados.

* Importante: Si no se determina la máscara, el comando asigna por defecto el valor 32.

## Ejemplo 

Para a un caso práctico, visualizar el video [ejemplo 1.1.1](/linux/Ejemplos/Parametros_Basicos/#111-comando-ip)
